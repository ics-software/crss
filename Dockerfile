FROM python:3.7-slim

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -g csi -u 1000 csi

COPY requirements.txt /requirements.txt
RUN python3 -m venv /venv \
  && /venv/bin/python3 -m pip install --no-cache-dir -r /requirements.txt \
  && chown -R csi:csi /venv

COPY --chown=csi:csi . /app/
WORKDIR /app

ENV PATH /venv/bin:$PATH
EXPOSE 8082
CMD ["python3", "/app/cr-setup.py"]

USER csi
