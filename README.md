# Control Room Shift Setup

A web application to setup information about shift and On-Call people.

## Development

### Without docker

1. Create a `.env` file at the root of the repository with the following variables:

    ```bash
    export LDAP_USER=<ldap_user>
    export LDAP_PASSWORD=<ldap_password>
    export EPICS_CA_ADDR_LIST=<ca addr list>
    ```

1. Create a python3 environment and install the requirements:

    ```bash
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ python3 -m pip install -r requirements.txt
    ```

1. Run the application:

    ```bash
    $ source .env
    $ python3 cr-setup.py
    ```

### With docker

1. Create a `.env` file at the root of the repository with the following variables
   (note that you should not use EXPORT with docker-compose):

    ```bash
    LDAP_USER=<ldap_user>
    LDAP_PASSWORD=<ldap_password>
    EPICS_CA_ADDR_LIST=<ca addr list>
    ```

1. Build the docker image:

   ```bash
   $ docker-compose build
   ```

1. Run the application:

   ```bash
   $ docker-compose up
   ```

In both cases, open your browser and go to http://localhost:8082

## Deployment

To deploy to production, you should tag and push to GitLab:

```bash
$ git tag -a <x.x.x>
$ git push --tags
```
