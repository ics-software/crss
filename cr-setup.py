from remi.gui import *
from remi import start, App
import epics
import ldap3
import time
import pytz
from datetime import datetime
import base64
import os


screens = {'Local Control Room' : {'Shift ID': {'PV': epics.PV('NSO-LCR:Ops:SID', auto_monitor=True), 'value': datetime.now(pytz.timezone('Europe/Stockholm')).strftime('%Y%m%d')+'A' if datetime.now(pytz.timezone('Europe/Stockholm')).hour<12 else datetime.now(pytz.timezone('Europe/Stockholm')).strftime('%Y%m%d')+'B', 'hint':'', 'search':False},
                                   'Message': {'PV': epics.PV('NSO-LCR:Ops:Msg', auto_monitor=True), 'value': '', 'hint': 'Write your message here', 'search':False},
                                   'Phone': {'PV': epics.PV('NSO-LCR:Ops:Phone', auto_monitor=True), 'value': '+46721792220', 'hint': '', 'search':False},
                                   'Shift Leader': {'PV': epics.PV('NSO-LCR:Ops:MSL', auto_monitor=True), 'value':'', 'hint':'Search', 'search':True},
                                   'Mobile Phone': {'PV': epics.PV('NSO-LCR:Ops:MSLPhone', auto_monitor=True), 'value': '', 'hint':'', 'search':False},
                                   'Email': {'PV': epics.PV('NSO-LCR:Ops:MSLEmail', auto_monitor=True), 'value':'', 'hint':'', 'search':False}},

           'Test Stand 2' :       {'Shift ID': {'PV': epics.PV('NSO-LCR:Ops:SID', auto_monitor=True), 'value': datetime.now(pytz.timezone('Europe/Stockholm')).strftime('%Y%m%d')+'A' if datetime.now(pytz.timezone('Europe/Stockholm')).hour<12 else datetime.now(pytz.timezone('Europe/Stockholm')).strftime('%Y%m%d')+'B', 'hint':'', 'search':False},
                                   'Test Stand 2 Message': {'PV': epics.PV('NSO-LCR:Ops:TSMsg', auto_monitor=True), 'value': '', 'hint': 'Write your message here', 'search':False},
                                   'Test Stand 2 Phone': { 'PV': epics.PV('NSO-LCR:Ops:TSPhone', auto_monitor=True), 'value': '+46721792220', 'hint':'', 'search':False},
                                   'Test Stand 2 Leader': {'PV': epics.PV('NSO-LCR:Ops:TSSL', auto_monitor=True), 'value':'', 'hint':'Search', 'search':True},
                                   'Mobile Phone': {'PV': epics.PV('NSO-LCR:Ops:TSMobile', auto_monitor=True), 'value':'', 'hint':'', 'search':False},
                                   'Email': {'PV': epics.PV('NSO-LCR:Ops:TSEmail', auto_monitor=True), 'value':'', 'hint':'', 'search':False}},

            'On-Call' :           {'On-Call Message': {'PV': epics.PV('NSO-LCR:Ops:OCMsg', auto_monitor=True), 'value': '', 'hint': 'Write your message here', 'search':False},
                                   'On-Call Phone': { 'PV': epics.PV('NSO-LCR:Ops:OCPhone', auto_monitor=True), 'value': '+46468883007', 'hint':'', 'search':False},
                                   'On-Call Leader': {'PV': epics.PV('NSO-LCR:Ops:OC', auto_monitor=True), 'value':'', 'hint':'Search', 'search':True},
                                   'Mobile Phone': {'PV': epics.PV('NSO-LCR:Ops:OCMobile', auto_monitor=True), 'value':'', 'hint':'', 'search':False},
                                   'Email': {'PV': epics.PV('NSO-LCR:Ops:OCEmail', auto_monitor=True), 'value':'', 'hint':'', 'search':False}},

            'On-Call Cryo' :      {'On-Call Cryo Message': {'PV': epics.PV('NSO-LCR:Ops:OCCMsg', auto_monitor=True), 'value': '', 'hint': 'Write your message here', 'search':False},
                                   'On-Call Cryo Phone': { 'PV': epics.PV('NSO-LCR:Ops:OCCPhone', auto_monitor=True), 'value': '+46468883166', 'hint':'', 'search':False},
                                   'On-Call Cryo Leader': {'PV': epics.PV('NSO-LCR:Ops:OCC', auto_monitor=True), 'value':'', 'hint':'Search', 'search':True},
                                   'Mobile Phone': {'PV': epics.PV('NSO-LCR:Ops:OCCMobile', auto_monitor=True), 'value':'', 'hint':'', 'search':False},
                                   'Email': {'PV': epics.PV('NSO-LCR:Ops:OCCEmail', auto_monitor=True), 'value':'', 'hint':'', 'search':False}},

            'On-Call Infra' :     {'On-Call Infra Message': {'PV': epics.PV('NSO-LCR:Ops:OCIMsg', auto_monitor=True), 'value': '', 'hint': 'Write your message here', 'search':False},
                                   'On-Call Infra Phone': { 'PV': epics.PV('NSO-LCR:Ops:OCIPhone', auto_monitor=True), 'value': '+46468883425', 'hint':'', 'search':False},
                                   'On-Call Infra Leader': {'PV': epics.PV('NSO-LCR:Ops:OCI', auto_monitor=True), 'value':'', 'hint':'Search', 'search':True},
                                   'Mobile Phone': {'PV': epics.PV('NSO-LCR:Ops:OCIMobile', auto_monitor=True), 'value':'', 'hint':'', 'search':False},
                                   'Email': {'PV': epics.PV('NSO-LCR:Ops:OCIEmail', auto_monitor=True), 'value':'', 'hint':'', 'search':False}},

            'On-Call PSS' :      {'On-Call PSS Message': {'PV': epics.PV('NSO-LCR:Ops:OCPSSMsg', auto_monitor=True), 'value': 'If no contact, contact On-call leader', 'hint': '', 'search':False},
                                   'On-Call PSS Phone': { 'PV': epics.PV('NSO-LCR:Ops:OCPSSPhone', auto_monitor=True), 'value': '+46468883278', 'hint':'', 'search':False},
                                   'On-Call PSS Leader': {'PV': epics.PV('NSO-LCR:Ops:OCPSS', auto_monitor=True), 'value':'', 'hint':'Search', 'search':True},
                                   'Mobile Phone': {'PV': epics.PV('NSO-LCR:Ops:OCPSSMobile', auto_monitor=True), 'value':'', 'hint':'', 'search':False},
                                   'Email': {'PV': epics.PV('NSO-LCR:Ops:OCPSSEmail', auto_monitor=True), 'value':'', 'hint':'', 'search':False}},

            'On-Call Elec' :     {'On-Call Elec Message': {'PV': epics.PV('NSO-LCR:Ops:OCEMsg', auto_monitor=True), 'value': 'The call will be received by NOKAS.', 'hint': '', 'search':False},
                                   'On-Call Elec Phone': { 'PV': epics.PV('NSO-LCR:Ops:OCEPhone', auto_monitor=True), 'value': '0771881000', 'hint':'', 'search':False},
                                   'On-Call Elec Leader': {'PV': epics.PV('NSO-LCR:Ops:OCE', auto_monitor=True), 'value':'', 'hint':'Search', 'search':True},
                                   'Mobile Phone': {'PV': epics.PV('NSO-LCR:Ops:OCEMobile', auto_monitor=True), 'value':'', 'hint':'', 'search':False},
                                   'Email': {'PV': epics.PV('NSO-LCR:Ops:OCEEmail', auto_monitor=True), 'value':'', 'hint':'', 'search':False}}}

class ldap():
    def __init__(self, *args, **kwargs):
        server = 'esss.lu.se'
        username = os.environ['LDAP_USER']
        password = os.environ['LDAP_PASSWORD']
        self.server = ldap3.Server(server)
        self.connection = ldap3.Connection(self.server, username, password)

    def search(self, field, text):
        search_dir = 'dc=esss,dc=lu,dc=se'
        attributes = ['telephoneNumber', 'CN', 'mail', 'mobile', 'pager', 'thumbnailPhoto']
        search_string = '(&('+field+'='
        for word in text.split():
            search_string+='*'+word+'*'
        search_string+= '))'
        
        self.connection.bind()
        if not self.connection.search(search_dir, search_string, attributes=attributes):
            return {}

        results = {}
        for result in self.connection.entries:
            name = result.cn.value
            results[name]={}
            if result.telephoneNumber.value == None:
                results[name]['phone'] = ''
            else:
                results[name]['phone'] = result.telephoneNumber.value.replace(' ','')
            if result.mobile.value == None:
                results[name]['mobile'] = ''
            else:
                results[name]['mobile'] = result.mobile.value.replace(' ','')
            if result.pager.value == None:
                results[name]['pager'] = ''
            else:
                results[name]['pager'] = result.pager.value.replace(' ','')
            if result.mail.value == None:
                results[name]['email'] = ''
            else:
                results[name]['email'] = result.mail.value

            if result.thumbnailPhoto.value == None:
                results[name]['photo'] = ''
            else:
                results[name]['photo'] = result.thumbnailPhoto.value
        self.connection.unbind()
        return (results)
            

class CRSetup(App):
    def __init__(self, *args, **kwargs):
        super(CRSetup, self).__init__(*args, static_file_path={'my_res':'./res/'})

    def main(self):
        time.sleep(1)
        return self.ui(list(screens.keys())[0])

    def on_close(self):
        self.connection.unbind()
        super(CRSetup, self).on_close()
        
    def menu_selector(self, widget, value):
        self.set_root_widget(self.ui(value))
        return

    def search_person(self, widget, value, screen):
        self.people = self.my_ldap.search('CN', value)
        self.drop_down_people.empty()
        for name in self.people.keys():
            self.drop_down_people.append(name)
        
        phone_index = list(screens[screen].keys()).index('Mobile Phone')
        email_index = list(screens[screen].keys()).index('Email')

        self.grid_dict['set_'+str(phone_index).zfill(6)].set_text('')
        self.grid_dict['set_'+str(email_index).zfill(6)].set_text('')
        self.grid_dict['set_photo'].children['image'].set_image('/my_res:no-photo.svg')

        if len(self.people) > 0:
            self.person_selector(None, list(self.people.keys())[0], screen)
        return

    def person_selector(self, widget, value, screen):
        phone_index = list(screens[screen].keys()).index('Mobile Phone')
        email_index = list(screens[screen].keys()).index('Email')

        self.grid_dict['set_'+str(phone_index).zfill(6)].set_text(self.people[value]['mobile'])
        self.grid_dict['set_'+str(email_index).zfill(6)].set_text(self.people[value]['email'])

        if len(self.people[value]['photo']) > 0:
            self.grid_dict['set_photo'].children['image'].set_image('data:image/png;base64, '+(base64.b64encode(self.people[value]['photo'])).decode('utf-8'))
        else:
            self.grid_dict['set_photo'].children['image'].set_image('/my_res:no-photo.svg')
        return

    def set_values(self, widget, screen):
        label_counter = 0
        for tag in screens[screen]:
            if screens[screen][tag]['PV'].connected and screens[screen][tag]['PV'].write_access:
                if screens[screen][tag]['search']:
                    if self.drop_down_people.get_value() == None:
                        screens[screen][tag]['PV'].put('')
                    else:
                        screens[screen][tag]['PV'].put(self.drop_down_people.get_value())
                else:
                        screens[screen][tag]['PV'].put((self.grid_dict['set_'+str(label_counter).zfill(6)].get_value()))
            label_counter+=1
        time.sleep(0.1)

        label_counter = 0
        for tag in screens[screen]:
            if screens[screen][tag]['PV'].connected:
                self.grid_dict['cur_'+str(label_counter).zfill(6)].set_text(screens[screen][tag]['PV'].value)   
            label_counter+=1

        self.grid_dict['cur_photo'].children['image'].set_image('/my_res:no-photo.svg')
        if screens[screen]['Email']['PV'].connected:
            personal_data = self.my_ldap.search('mail', screens[screen]['Email']['PV'].value)
            if len(personal_data)>0:
                photo = personal_data[list(personal_data.keys())[0]]['photo']
                if len(photo) > 0:
                    self.grid_dict['cur_photo'].children['image'].set_image('data:image/png;base64, '+(base64.b64encode(photo)).decode('utf-8'))
        return

    def ui(self, screen):
        self.my_ldap = ldap()

        my_css = '<link rel="stylesheet" href="/my_res:ess.css">'
        self.page.children['head'].add_child('my_css', my_css)
        self.page.children['body'].css_background_color ='#0c232d'

        self.header = Svg()
        self.header.attr_viewBox = "0,0,200,20"
        self.header.style.update({'width':'100%', 'height':'100%'})
        header_rectangle = SvgRectangle(0,0,100,20)
        header_rectangle.set_style({'width':'100%', 'height':'100%', 'align':'auto'})
        header_rectangle.set_fill(color='#0B2d3c')
        text_rectangle = SvgRectangle(0,4,100,20)
        text_rectangle.set_style({'width':'100%', 'height':'100%', 'align':'auto'})
        text_rectangle.set_fill(color='#123B4A')
        text = SvgText(55, 15, 'Control Room Shift Setup')
        text.attr_fill = '#ffffff'
        text.set_style({'font-family':'Titillium Light', 'font-size':'10px'})
        logo = SvgImage(image_data='/my_res:ess-logo.svg', x=0, y=-5.5, w=35, h=35)

        self.header.append(header_rectangle)
        self.header.append(text_rectangle)
        self.header.append(text)
        self.header.append(logo)

        self.main_selection = DropDown()
        for i in screens:
            self.main_selection.append(i)
        self.main_selection.set_value(screen)
        self.main_selection.set_style({'width':'auto','font-size':'20px', 'font-family':'Titillium Light', 'color':'#ffffff', 'background': '#123B4A', 'margin':'auto'})
        self.main_selection.onchange.do(self.menu_selector)
    
        self.grid_dict = {}
        self.grid_dict['header'] = self.header
        self.grid_dict['selector'] = self.main_selection
        self.grid_dict['cur_label'] = Label('Current Value', style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px', 'text-align':'center'})
        self.grid_dict['set_label'] = Label('Set Value', style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px', 'text-align':'center'})
        self.grid_dict['lbl_photo'] = Label('Photo',style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px'})
        
        self.grid_dict['cur_photo'] = VBox(children={'image':Image('/my_res:no-photo.svg', style={'background': '#123B4A', 'width':'auto', 'height':'100%', 'max-width':'100px'})}, width="100%", height="100%", style={'background-color':'#123B4A'})
        
        if screens[screen]['Email']['PV'].connected:
            personal_data = self.my_ldap.search('mail', screens[screen]['Email']['PV'].value)
            if len(personal_data)>0:
                photo = personal_data[list(personal_data.keys())[0]]['photo']
                if len(photo) > 0:
                    self.grid_dict['cur_photo'].children['image'].set_image('data:image/png;base64, '+(base64.b64encode(photo)).decode('utf-8'))

        self.grid_dict['set_photo'] = VBox(children={'image':Image('/my_res:no-photo.svg', style={'background': '#123B4A', 'width':'auto', 'height':'100%', 'max-width':'100px'})}, width="100%", height="100%", style={'background-color':'#123B4A'})

        container = GridBox()
        container.set_style({'background':'#0c232d', 'width':'100%', 'height':'100%', })
        grid_table = """
            | header     | header     | header     |
            |            | selector   |            |
            |            | cur_label  | set_label  |
        """
        label_counter = 0
        for tag in screens[screen]:
            self.grid_dict['lbl_'+str(label_counter).zfill(6)] = Label(tag, style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px'})
            if screens[screen][tag]['PV'].connected:
                value = screens[screen][tag]['PV'].value
            else:
                value = 'Offline'    
            self.grid_dict['cur_'+str(label_counter).zfill(6)] = Label(value, style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px'})

            if screens[screen][tag]['search']:
                search_box = TextInput(single_line = True, hint=screens[screen][tag]['hint'], style={'color':'#ffffff', 'background': '#151b1d', 'font-family':'Titillium Light', 'font-size':'20px', 'height':'50%'})
                search_box.set_value(screens[screen][tag]['value'])
                search_box.onchange.do(self.search_person, screen)
                self.drop_down_people = DropDown(style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px'})
                self.drop_down_people.onchange.do(self.person_selector, screen)
                self.grid_dict['set_'+str(label_counter).zfill(6)] = VBox([search_box, self.drop_down_people], style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px'})
            else:
                self.grid_dict['set_'+str(label_counter).zfill(6)] = TextInput(single_line = True, hint = screens[screen][tag]['hint'], style={'color':'#ffffff', 'background': '#151b1d', 'font-family':'Titillium Light', 'font-size':'20px'})
                self.grid_dict['set_'+str(label_counter).zfill(6)].attributes['maxlength'] = '40'
                self.grid_dict['set_'+str(label_counter).zfill(6)].set_value(screens[screen][tag]['value'])
            grid_table += '| '
            grid_table += 'lbl_'+str(label_counter).zfill(6)
            grid_table += ' |'
            grid_table += 'cur_'+str(label_counter).zfill(6)
            grid_table += ' |'
            grid_table += 'set_'+str(label_counter).zfill(6)
            grid_table += ' |\n'
            label_counter += 1

        grid_table+="""
            | lbl_photo  | cur_photo  | set_photo  |
            |            | set_button |            |
        """
        
        container.set_from_asciiart(grid_table, column_gap=0.5, row_gap=0.5)
        
        self.set_button = Button('Set New Values', style={'color':'#ffffff', 'background': '#123B4A', 'font-family':'Titillium Light', 'font-size':'20px', 'margin':'auto', 'box-shadow': 'none', 'padding' : '0px 10px 0px 10px'})
        self.set_button.onclick.do(self.set_values, screen)
        self.grid_dict['set_button'] = self.set_button
        container.append(self.grid_dict)
        return container

if __name__ == "__main__":
    start(CRSetup,address='0.0.0.0', port=8082, multiple_instance=False, enable_file_cache=False, update_interval=0.1, start_browser=False)
